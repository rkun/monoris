window.onload = function(){
	var cssRenderer, scene, camera, imageObj;

	init();
	animate();

	//初期配置
	function init(){
		// Initialize cssRenderer
		cssRenderer = new THREE.CSS3DRenderer();
		cssRenderer.setSize(window.innerWidth, window.innerHeight);
		cssRenderer.domElement.style.position = "absolute";
		cssRenderer.domElement.style.top = 0;
		cssRenderer.domElement.style.left = 0;
		scene = new THREE.Scene();

		placer(12);
		
		camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
		camera.position.set(0, 0, 0);
		scene.add(camera);

		document.body.appendChild(cssRenderer.domElement);
		cssRenderer.render(scene, camera);

		/*
		document.body.addEventListener("mousemove", function(e){
			camera.rotation.y = e.clientX / window.innerWidth * 2 * Math.PI - Math.PI;
		});
		*/
	}

	// animate
	function animate(){
		camera.rotation.y += 0.01;
		cssRenderer.render(scene, camera);
		window.requestAnimationFrame(animate);
	}
	
	// 再配置
	function placer(cnt){
		var objects = [];
		var theta = Math.PI * 2 / cnt;
		var distance = 1500;
		for(var i=0; i < cnt; i++){
			var obj = generateObject(i); 
			obj.position.set(objPosition(theta * i, distance).x, 0,  objPosition(theta * i, distance).z);
			obj.rotation.y = theta * i;
			scene.add(obj);
			objects.push(obj);
		}
		console.log(objects);
		function objPosition(_theta, _distance){
			return {
				x: Math.sin(_theta) * -_distance,
				z: Math.cos(_theta) * -_distance
			}
		}
	}

	//オブジェクト作成
	function generateObject(_number){
		var wrapperElem = document.createElement("div");
		wrapperElem.innerHTML = "<h6>SOUND ONLY</h6><h4>"+_number+"</h4>";
		wrapperElem.style = "text-align: center; color: red; font-size: 100px; background-color: black;";
		var imageElem = document.createElement("img");
		imageElem.src = "./image.png";
		wrapperElem.appendChild(imageElem);
		var elemObj = new THREE.CSS3DObject(wrapperElem);
		return elemObj;
	}
}
