import redis from "redis";
let client = redis.createClient();

client.on("error", e => {
	console.log(`Error occured ${e}`);
});

export default client;
