import Hapi from "hapi";
import Nes from "nes";
import request from "request";
import Discord from "discord.js";
import socketio from "socket.io";
import querystring from "querystring";
import path from "path";
import inert from "inert";
import redis from "./redis.js";
import uuid from "uuid/v1";
import fs from "fs";

"use strict";

const PORT = 2000;

const BOT_TOKEN = "NTIyMDM3MjI4NjU1MDgzNTMx.Dvut2Q.X6BF6QSHE9S23KoYquAUWZpavcw";

const cli = new Discord.Client();


// Hapi
let server = Hapi.server({
	port: PORT,
	host: "localhost"
})

// Socket.IO
let io = socketio(server.listener);
io.on("connection", () => {
	console.log("client connected");
});

const initDiscord = () => {
	cli.login(BOT_TOKEN).then(() => {
		console.log("Loaded!!");
		let VCs = [];
		for(let i in cli.channels.array()){
			const channel = cli.channels.array()[i];
			if(channel.type == "voice" && channel.members.size){
				console.log(channel.members.array()[0].user.username);
				VCs.push(channel);
			}
		}
		//console.log(VCs);
	});
	console.log("Initialize members...");

	// 呼び出しコマンド受信処理
	cli.on("message", (message, _) => {
		const channel_id = message.channel.guild.id;
		const command = message.content;
		if(command.match(/^\!(hey|HEY)$/)){
			// コマンドを受けたギルドのGeneralなVoiceチャンネルのIDを探索
			const guild_channels = message.channel.client.channels;
			guild_channels.array().reduce((_, channel) => {
				if(channel.type == voice && channel.name == "General") channel_id = channel.id;
			});

			message.reply(`\nOK. I'll stand up from the chair.\nYou can visit http://localhost:2000/app?id=${guild_id}`);
		}
	});
	
	// ボイスチャンネルへのメンバー参加時
	cli.on("voiceStateUpdate", (old_member, new_member) => {
		if(!old_member.voiceChannel && new_member.voiceChannel && new_member.voiceChannel.name == "General"){
			const channel_id = new_member.voiceChannelID;
			const guild_id = new_member.guild.id;
			const user_id = new_member.user.id;
			const username = new_member.user.username;
			console.log(`guild_id: ${guild_id}\nuser_id: ${user_id}\nusername: ${username}\nchannel_id: ${channel_id}`);
			console.log("Join");
			const io_body = {
				"flag": 1,
				"id": user_id,
				"username": username 
			};
			io.emit(channel_id, io_body);
			
		}else if(old_member.voiceChannel && !new_member.voiceChannel && old_member.voiceChannel.name == "General"){
			const channel_id = old_member.voiceChannelID;
			const guild_id = old_member.guild.id;
			const user_id = old_member.user.id;
			const username = old_member.user.username;
			console.log(`guild_id: ${guild_id}\nuser_id: ${user_id}\nusername: ${username}\nchannel_id: ${channel_id}`);
			console.log("Leave");
			const io_body = {
				"flag": 0,
				"id": user_id,
				"username": username 
			};
			io.emit(channel_id, io_body);
		}
	});
}
initDiscord();

const init = async () => {
	await server.register({
		plugin: inert
	})
	await server.register(Nes);

	await server.start();
	console.log(`Server running at ${server.info.uri}`);
}

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init().then(() => {
	console.log("Server start up Sequence has up.");
})


// Index site routing
server.route({
	method: "GET",
	path: "/",
	handler: (req, h) => {
		let file = fs.readFileSync(path.join(__dirname, "../../public/index.html"));
		let res = h.response(file);
		res.type("text/html");
		return res;
	}
})

// Main app routing
server.route({
	method: "GET",
	path: "/app",
	handler: (req, h) => {
		//redis.set(id, code);
		let file = fs.readFileSync(path.join(__dirname, "../../public/hastle.html"));
		let res = h.response(file);
		res.type("text/html");
		return res;
	}
})

// すでに参加しているメンバーの人数を提供
server.route({
	method: "GET",
	path: "/app/{id}",
	handler: (req, h) => {
		const dict = {
			amount: 0
		};
		cli.channels.array().reduce((_, value) => {
			if(value.id == req.params.id) dict.amount = value.members.size;
		});
		return dict;
	}
})

// Static supplier
server.route({
	method: "GET",
	path: "/static/{file*}",
	handler: {
		directory: {
			path: path.join(__dirname, "../../public/static"),
			listing: true
		}
	}
});
